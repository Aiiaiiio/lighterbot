extends StaticBody

enum State { Idle, MovingForward, Turning, Jumping, Interacting, Confused }

export var boredFrequencyMinSec = 3
export var boredFrequencyMaxSec = 8
export var animSpeed = 2.0

onready var mState = State.Idle
onready var mAudio = $AudioStreamPlayer

var mCurrentAudioStream = ""
var mAudioStreamPositon = 0.0
var mTweenTargets = []
var mProgramFunctions = []
var mCallstack = []
var mInitialGlobalTransform
var mCelebrationFlag = false

const FUNC_PREFIX = "fn_"
const WALK_SOUND = [ "res://sfx/walk.ogg", true ]
const TURN_SOUND = [ "res://sfx/turn.ogg", false ]
const JUMP_SOUND = [ "res://sfx/jump.ogg", false ]
const ERROR_SOUND = [ "res://sfx/error.ogg", false ]

func executeNextCommand():
	if !mCelebrationFlag:
		get_tree().call_group( "gui", "onExecution", mCallstack )
		yield( get_tree(), "idle_frame" )
	
	var next_call = mCallstack.back()
	var func_idx = next_call[0]
	var cmd_idx = next_call[1]
	
	if cmd_idx < len( mProgramFunctions[ func_idx ] ):
		var cmd = mProgramFunctions[ func_idx][ cmd_idx ]
		if cmd.substr( 0, len( FUNC_PREFIX ) ) == FUNC_PREFIX:
			var sub_func_idx = int( cmd.substr( len( FUNC_PREFIX ) ) )
			next_call[1] += 1
			mCallstack.append( [ sub_func_idx, 0 ] )
			executeNextCommand()
		else:
			get_tree().call_group( "logic", "doCommand", mProgramFunctions[ func_idx ][ cmd_idx ] )
			next_call[1] += 1
	else:
		while !mCallstack.empty() && ( cmd_idx >= len( mProgramFunctions[ func_idx ] ) ):
			mCallstack.pop_back()
			if !mCallstack.empty():
				next_call = mCallstack.back()
				func_idx = next_call[0]
				cmd_idx = next_call[1]
			
		if mCallstack.empty():
				if mCelebrationFlag:
					get_tree().call_group( "logic", "onExecutionfinished", mCelebrationFlag )
					mCelebrationFlag = false
				else:
					get_tree().call_group( "gui", "onExecutionfinished", mCelebrationFlag )
					get_tree().call_group( "logic", "onExecutionfinished", mCelebrationFlag )
		else:
			executeNextCommand()
	
func executeProgram():
	if isInProgrammingState():
		mCelebrationFlag = false
		global_transform = mInitialGlobalTransform
		yield( get_tree(), "idle_frame" )
		if len( mProgramFunctions ) > 0:
			mCallstack = [ [ 0, 0 ] ]
			executeNextCommand()
			
func executeCelebrationProgram():
	if isInProgrammingState():
		mCelebrationFlag = true
		clearProgram()
		queueCommand( 0, "right" )
		queueCommand( 0, "right" )
		queueCommand( 0, "right" )
		queueCommand( 0, "right" )
		mCallstack = [ [ 0, 0 ] ]
		executeNextCommand()
		
func clearProgram():
	if isInProgrammingState():
		mCallstack = []
		mProgramFunctions = []

func isInProgrammingState():
	return mCallstack.empty()

func queueCommand( func_index, command ):
	if !isInProgrammingState():
		return
		
	while func_index >= len( mProgramFunctions ):
		mProgramFunctions.append( [] )
	mProgramFunctions[ func_index ].append( command )
	print( "Command %s queued into function %s" % [ command, func_index ] )

func popCommand( func_index ):
	if !isInProgrammingState():
		return
		
	if func_index < len( mProgramFunctions ):
		mProgramFunctions[ func_index ].pop()
	
func clearFunction( func_index = -1 ):
	if !isInProgrammingState():
		return
		
	if func_index > -1:
		if func_index < len( mProgramFunctions ):
			mProgramFunctions[ func_index ] = []
	else:
		mProgramFunctions = []

func setAudioStream( stream_res_url ):
	if mCurrentAudioStream != stream_res_url[0]:
			mCurrentAudioStream = stream_res_url[0]
			mAudio.stream = load( mCurrentAudioStream )
			mAudioStreamPositon = 0
			
	if !stream_res_url[1]:
			mAudioStreamPositon = 0        

func _ready():
	mInitialGlobalTransform = global_transform
	randomize()
	$Timer.wait_time = getNewWaitTime()
	$Timer.start()

func getNewWaitTime():
	return boredFrequencyMinSec + ( randi() % ( boredFrequencyMaxSec - boredFrequencyMinSec ) )

#func _process( __ ):
#    if Input.is_action_just_pressed( "test_forward" ):
#        get_tree().call_group( "logic", "doCommand", "forward" )
#    elif Input.is_action_just_pressed( "test_left" ):
#        get_tree().call_group( "logic", "doCommand", "left" )
#    elif Input.is_action_just_pressed( "test_right" ):
#        get_tree().call_group( "logic", "doCommand", "right" )
#    elif Input.is_action_just_pressed( "test_interact" ):
#        get_tree().call_group( "logic", "doCommand", "interact" )
#    elif Input.is_action_just_pressed( "test_jump" ):
#        get_tree().call_group( "logic", "doCommand", "jump" )

func _on_Timer_timeout():
	if mState == State.Idle:
		$AnimationPlayer.play( "Idle" )
	$Timer.wait_time = getNewWaitTime()
	$Timer.start()

func walkForward():
	if mState == State.Idle:
		mState = State.MovingForward
		
		setAudioStream( WALK_SOUND )
		
		var diff = $Forward.global_transform.origin - global_transform.origin
		mTweenTargets.push_back( { "prop": "translation", "diff": Vector3( diff.x, 0.0, diff.z ), "length": $AnimationPlayer.get_animation( "Walk2" ).length * (1.0/animSpeed), "trans_type": Tween.TRANS_LINEAR, "easing": Tween.EASE_OUT } )
		
		$AnimationPlayer.play( "Walk2", -1, animSpeed )
		advanceTween()
		mAudio.play( mAudioStreamPositon )
		
func jump( level_diff ):
	if mState == State.Idle:
		mState = State.Jumping

		setAudioStream( JUMP_SOUND )

		var diff = $Forward.global_transform.origin - global_transform.origin
		
		mTweenTargets.push_back( { "prop": "translation", "diff": Vector3.ZERO, "length": 0.4 * (1.0/animSpeed), "trans_type": Tween.TRANS_LINEAR, "easing": Tween.EASE_IN } )
		mTweenTargets.push_back( { "prop": "translation", "diff": Vector3( diff.x, 1.5, diff.z ), "length": 0.6 * (1.0/animSpeed), "trans_type": Tween.TRANS_SINE, "easing": Tween.EASE_OUT } )
		mTweenTargets.push_back( { "prop": "translation", "diff": Vector3( 0, -( 1.5 - ( level_diff * 1.0 ) ), 0 ), "length": ( abs( level_diff ) + 1 ) * 0.1 * (1.0/animSpeed), "trans_type": Tween.TRANS_SINE, "easing": Tween.EASE_IN } )
		
		$AnimationPlayer.play( "Jump1", -1, animSpeed )
		advanceTween()
		mAudio.play( mAudioStreamPositon )

func turnRight():
	if mState == State.Idle:
		mState = State.Turning
		
		setAudioStream( TURN_SOUND )
		
		mTweenTargets.push_back( { "prop": "rotation", "diff": Vector3( 0.0, deg2rad( -90 ), 0.0 ), "length": $AnimationPlayer.get_animation( "TurnHop" ).length * (1.0/animSpeed), "trans_type": Tween.TRANS_SINE, "easing": Tween.EASE_IN } )
		$AnimationPlayer.play( "TurnHop", -1, animSpeed )
		advanceTween()
		mAudio.play( mAudioStreamPositon )
	
func turnLeft():
	if mState == State.Idle:
		mState = State.Turning
		
		setAudioStream( TURN_SOUND )
		
		mTweenTargets.push_back( { "prop": "rotation", "diff": Vector3( 0.0, deg2rad( 90 ), 0.0 ), "length": $AnimationPlayer.get_animation( "TurnHop" ).length * (1.0/animSpeed), "trans_type": Tween.TRANS_SINE, "easing": Tween.EASE_IN } )
		$AnimationPlayer.play( "TurnHop", -1, animSpeed )
		advanceTween()
		mAudio.play( mAudioStreamPositon )
		
func showConfusion():
	if mState == State.Idle:
		mState = State.Confused
		setAudioStream( ERROR_SOUND )
		$AnimationPlayer.play( "Idle", -1, animSpeed )
		mAudio.play( mAudioStreamPositon )
	
func interact():
	if mState == State.Idle:
		mState = State.Interacting
		$AnimationPlayer.play( "Interact", -1, animSpeed )
		get_tree().call_group( "interactive_tile", "toggleLight" )

func advanceTween():
	if mTweenTargets.size() > 0:
		var target = mTweenTargets.pop_front()
		var tween = $Tween
		var start = translation
		if target["prop"] == "rotation":
			start = rotation
		var diff = target[ "diff" ]
		var end = start + diff
		tween.interpolate_property( self, target[ "prop" ], start, end, target[ "length" ], target[ "trans_type" ], target[ "easing" ] )
		tween.start()

func _on_AnimationPlayer_animation_finished( __ ):
	mAudioStreamPositon = mAudio.get_playback_position()
	mAudio.stop()
	mState = State.Idle
	if !isInProgrammingState():
		executeNextCommand()

func _on_Tween_tween_all_completed():
	advanceTween()

func tileInFrontOfBotCoords():
	var diff = $Forward.global_transform.origin - global_transform.origin
	return Vector3( global_transform.origin.x + diff.x, global_transform.origin.y - 1.5, global_transform.origin.z + diff.z )
	
func tileOfBotCoords():
	return Vector3( global_transform.origin.x, global_transform.origin.y - 1.5, global_transform.origin.z )
