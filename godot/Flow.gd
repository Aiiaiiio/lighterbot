extends Node

signal profileChanged()
signal profileListChanged()
signal progressChanged()

const LAST_PROFILE_FILE = "user://last_profile.json"

var mUserProfileFileName = "profile_1.json"
var mUserProfileDisplayName = "Player 1"
var mUserProgress = []

var mUserProfileList = []
onready var mLevelFileList = []

func getLevelFileList():
	return mLevelFileList

func getUserProfileList():
	return mUserProfileList

func levelCompleted( level_scene_path ):
	get_tree().change_scene_to( load( "res://GUI/MainScreen.tscn" ) )
	var level_info = findLevelInfo( level_scene_path )
	if level_info == null:
		mUserProgress.append( { "levelPath": level_scene_path, "isCompleted": true } )
	else:
		level_info[ "isCompleted" ] = true
	_saveProfile()
	emit_signal( "progressChanged" )
	
func findLevelInfo( level_scene_path ):
	for level_info in mUserProgress:
		if level_info[ "levelPath" ] == level_scene_path:
			return level_info
	return null
	
func startLevel( scene_path ):
	get_tree().change_scene_to( load( scene_path ) )

func getProfileDisplayNameFor( profile_file ):
	var file = File.new()
	if file.open( profile_file, File.READ ) == OK:
		var json_string = file.get_as_text()
		file.close()
		var data = JSON.parse( json_string ).result
		return data[ "displayName" ]
	else:
		printerr( "Failed to load display name: " + profile_file )
		return "???"

func getDisplayNameForCurrentProfile():
	return mUserProfileDisplayName
	
func getFileNameForCurrentProfile():
	return mUserProfileFileName

func createNewProfile( profile_name ):
	var profile_data = { "displayName": profile_name, "userProgress": [] }
	var file_name = _createFileNameForProfile()
	var json_string = JSON.print( profile_data, "  " )
	print( "Ez a fileba: " + json_string )
	
	var file = File.new()
	if file.open( _getSaveFilePathForProfileFileName( file_name ), File.WRITE ) == OK:
		file.store_string( json_string )
		file.close()
		print( "New profile progress saved: " + file_name )
		mUserProfileList.append( { "fileName": file_name.get_file(), "displayName": profile_name } )
		emit_signal( "profileListChanged" )
		return file_name
	else:
		printerr( "Failed to save progress: " + file_name )
		
	return ""

func changeProfileName( new_name ):
	if mUserProfileDisplayName != new_name:
		mUserProfileDisplayName = new_name
		_saveProfile()
		_loadProfileInfos()
	
func switchProfile( profile_file_name ):
	if mUserProfileFileName != profile_file_name:
		_saveProfile()
		mUserProfileFileName = profile_file_name
		_loadProfile()

func resetProfile():
	mUserProgress = []
	_saveProfile()

func getProgressForLevel( level_path ):
	for level_info in mUserProgress:
		if level_info[ "levelPath" ] == level_path:
			return level_info[ "isCompleted" ]

func removeCurrentProfile():
	var profile_to_select = 0
	if mUserProfileList.size() > 1:
		for i in range( 0, mUserProfileList.size() ):
			if mUserProfileList[i][ "fileName" ] == mUserProfileFileName:
				profile_to_select = i
				break
		
		if profile_to_select + 1 == mUserProfileList.size():
			profile_to_select = max( 0, profile_to_select - 1 )

	if Directory.new().remove( _getSaveFilePath() ) == OK:
		_loadProfileInfos()
		if !mUserProfileList.empty():
			mUserProfileFileName = mUserProfileList[ profile_to_select ][ "fileName" ]
			_loadProfile()
		else:
			_createDefaultProfile()
	else:
		print( "Failed to remove file: " + mUserProfileFileName )

func _ready():
	_loadLevelList()
	_loadProfileInfos()
	_loadLastUsedProfile()

class LevelFileNameComparer:
	static func compare( a, b ):
		var idx_a = a.get_file().get_basename().to_int()
		var idx_b = b.get_file().get_basename().to_int()
		if idx_a * idx_b == 0:
			return a < b
		elif idx_a <= 0:
			return false
		elif idx_b <= 0:
			return true
		return idx_a < idx_b

func _loadLevelList():
	mLevelFileList = FileGrabber.getFilesInFolder( "res://LevelsToPlay/" )
	mLevelFileList.sort_custom( LevelFileNameComparer, "compare" )

func _loadProfileInfos():
	mUserProfileList = []
	var file_list = FileGrabber.getFilesInFolder( "user://" )
	for file in file_list:
		if file.get_extension() == "json" && file.get_file().begins_with( "profile_" ):
			var f = File.new()
			if f.open( file, File.READ ) == OK:
				var json_string = f.get_as_text()
				f.close()
				var data = JSON.parse( json_string ).result
				mUserProfileList.append( { "fileName": file.get_file(), "displayName": data[ "displayName" ] } )
	emit_signal( "profileListChanged" )

func _loadLastUsedProfile():
	
	
	var file = File.new()
	if file.file_exists( LAST_PROFILE_FILE ):
		if file.open( LAST_PROFILE_FILE, File.READ ) == OK:
			var json_string = file.get_as_text()
			file.close()
			var data = JSON.parse( json_string ).result
			mUserProfileFileName = data[ "lastProfileUsed" ]
			print( "Last profile used: " + mUserProfileFileName )
			if !_loadProfile():
				if mUserProfileList.empty():
					_createDefaultProfile()
				else:
					mUserProfileFileName = mUserProfileList[0][ "fileName" ]
					_loadProfile()
		else:
			printerr( "Failed to load last profile: " + _getSaveFilePath() )
			_createDefaultProfile()
	else:
		_createDefaultProfile()
		
func _saveLastUsedProfile():
	var data = { "lastProfileUsed": mUserProfileFileName }
	var json_string = JSON.print( data, "  " )
	
	var file = File.new()
	if file.open( LAST_PROFILE_FILE, File.WRITE ) == OK:
		file.store_string( json_string )
		file.close()
		print( "Last profile used saved: " + _getSaveFilePath() )
	else:
		printerr( "Failed to save last used profile" )

func _createDefaultProfile():
	mUserProfileDisplayName = "Player 1"
	mUserProfileFileName = _createFileNameForProfile()
	mUserProgress = []
	_saveProfile()
	_saveLastUsedProfile()
	_loadProfileInfos()
	emit_signal( "profileChanged" )

func _createFileNameForProfile():
	var file = File.new()
	var idx = 1
	while file.file_exists( "user://profile_%s.json" % idx ):
		idx += 1
	return ( "profile_%s.json" % idx )

func _getSaveFilePath():
	return _getSaveFilePathForProfileFileName( mUserProfileFileName )
	
func _getSaveFilePathForProfileFileName( profile_file_name ):
	return "user://" + profile_file_name

func _saveProfile():
	var profile_data = { "displayName": mUserProfileDisplayName, "userProgress": mUserProgress }
	var json_string = JSON.print( profile_data, "  " )
	
	var file = File.new()
	if file.open( _getSaveFilePath(), File.WRITE ) == OK:
		file.store_string( json_string )
		file.close()
		print( "Progress saved: " + _getSaveFilePath() )
	else:
		printerr( "Failed to save progress: " + _getSaveFilePath() )
	
func _loadProfile():
	var file = File.new()
	if file.open( _getSaveFilePath(), File.READ ) == OK:
		var json_string = file.get_as_text()
		file.close()
		var data = JSON.parse( json_string ).result
		mUserProfileDisplayName = data[ "displayName" ]
		mUserProgress = data[ "userProgress" ]
		print( "Progress loaded: " + _getSaveFilePath() )
		_saveLastUsedProfile()
		emit_signal( "profileChanged" )
		emit_signal( "progressChanged" )
		return true
	else:
		printerr( "Failed to load progress: " + _getSaveFilePath() )
		return false
