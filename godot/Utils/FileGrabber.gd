extends Node

func getFilesInFolder( folder ):
	var gathered_files = []
	var dir = Directory.new()

	dir.open( folder )
	dir.list_dir_begin( true )

	var file = dir.get_next()
	while file != "":
		gathered_files.append( folder + file )
		file = dir.get_next()

	return gathered_files
