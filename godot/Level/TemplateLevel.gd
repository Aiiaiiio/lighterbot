extends Spatial

var mLightsNum = 0
var mLightsLit = 0

var mMap = []
var mMapBounds = [ [ 0, 0 ], [ 0, 0 ] ] # x min/max, z min/max
var mToZeroTranslate = [ 0.0, 0.0 ]

const STEP_X = 2.0
const STEP_Y = 1.0
const STEP_Z = 2.0

onready var mBot = $Bot

func _ready():
	mLightsNum = $InteractiveTiles.get_child_count()
	discoverMapGeometry()

func onExecutionfinished( celebration_flag ):
	if celebration_flag:
		print( "Level completed!" )
		Flow.levelCompleted( filename )
	else:
		if mLightsLit == mLightsNum:
			get_tree().call_group( "bot", "executeCelebrationProgram" )

func globalPosToLogic( transform_origin ):
	return Vector3( round( transform_origin.x / STEP_X - mToZeroTranslate[0] ), round( transform_origin.y / STEP_Y ), round( transform_origin.z / STEP_Z - mToZeroTranslate[1] ) )
	
func withinBounds( logic_coords ):
	return  ( logic_coords.x >= mMapBounds[0][0] ) && \
			( logic_coords.x <= mMapBounds[0][1] ) && \
			( logic_coords.z >= mMapBounds[1][0] ) && \
			( logic_coords.z <= mMapBounds[1][1] )
			
func heightOfTile( logic_coords ):
	if !withinBounds( logic_coords ):
		return null
	return mMap[ logic_coords.z ][ logic_coords.x ]

func onLightToggled( state ):
	if state:
		mLightsLit += 1
	else:
		mLightsLit -= 1
	print( "Lights lit: %s / %s" % [ mLightsLit, mLightsNum ] )

func resetLevel():
	for tile in $InteractiveTiles.get_children():
		tile.reset()
	mLightsLit = 0

func discoverMapGeometry():
	var tiles = $Tiles.get_children() + $InteractiveTiles.get_children()
	if len( tiles ) > 0:
		mToZeroTranslate = [ 0.0, 0.0 ]
		var first_tile_coords = globalPosToLogic( tiles[0].topCoords() )
		mMapBounds[ 0 ] = [ first_tile_coords.x, first_tile_coords.x ]
		mMapBounds[ 1 ] = [ first_tile_coords.z, first_tile_coords.z ]
		for tile in tiles:
			var t = globalPosToLogic( tile.topCoords() )
			if t.x < mMapBounds[0][0]:
				mMapBounds[0][0] = t.x
			if t.x > mMapBounds[0][1]:
				mMapBounds[0][1] = t.x
				
			if t.z < mMapBounds[1][0]:
				mMapBounds[1][0] = t.z
			if t.z > mMapBounds[1][1]:
				mMapBounds[1][1] = t.z
				
		mToZeroTranslate = [ mMapBounds[0][0], mMapBounds[1][0] ]
		mMapBounds[0] = [ mMapBounds[0][0] - mToZeroTranslate[0], mMapBounds[0][1] - mToZeroTranslate[0] ]
		mMapBounds[1] = [ mMapBounds[1][0] - mToZeroTranslate[1], mMapBounds[1][1] - mToZeroTranslate[1] ]
		
		mMap = []
		for row in range( 0, mMapBounds[1][1] - mMapBounds[1][0] + 1 ):
			mMap.push_back( [] )
			for __ in range( 0, mMapBounds[0][1] - mMapBounds[0][0] + 1 ):
				mMap[ row ].push_back( null )

		for tile in tiles:
			var logic_pos = globalPosToLogic( tile.global_transform.origin )
			mMap[ logic_pos.z ][ logic_pos.x ] = logic_pos.y
		 
		#Print map   
#        for row in range( 0, mMapBounds[1][1] - mMapBounds[1][0] + 1 ):
#            print( str( mMap[ row ] ) )

func doCommand( command ):
	match command:
		"forward":
			forwardCommand()
		"jump":
			jumpCommand()
		"right":
			turnRightCommand()
		"left":
			turnLeftCommand()
		"interact":
			interactCommand()
			
func forwardCommand():
	var target_logic_pos = globalPosToLogic( mBot.tileInFrontOfBotCoords() )
	var target_tile_height = heightOfTile( target_logic_pos )
	if target_tile_height != null:
		var bot_logic_pos = globalPosToLogic( mBot.tileOfBotCoords() )
		var bot_tile_height = mMap[ bot_logic_pos.z ][ bot_logic_pos.x ]
		if target_tile_height == bot_tile_height:
			mBot.walkForward()
			return
	mBot.showConfusion()
	
func turnRightCommand():
	mBot.turnRight()

func turnLeftCommand():
	mBot.turnLeft()
	
func jumpCommand():
	var target_logic_pos = globalPosToLogic( mBot.tileInFrontOfBotCoords() )
	var target_tile_height = heightOfTile( target_logic_pos )
	if target_tile_height != null:
		var bot_logic_pos = globalPosToLogic( mBot.tileOfBotCoords() )
		var bot_tile_height = heightOfTile( bot_logic_pos )
		if ( target_tile_height - 1 == bot_tile_height ) || ( target_tile_height <= bot_tile_height ):
			mBot.jump( target_tile_height - bot_tile_height )
			return
			
	mBot.showConfusion()
	
func interactCommand():
	mBot.interact()
