extends "res://Level/Tile.gd"

var mBotOnTop = false
var mLit = false

func toggleLight():
	if mBotOnTop:
		mLit = !mLit
		if mLit:
			$AudioStreamPlayer.stream = load( "res://sfx/turnon.ogg" )
			$Surface.material_override = load( "res://Level/Tile_interactive_on_material.tres" )
			$AudioStreamPlayer.play()
		else:
			$AudioStreamPlayer.stream = load( "res://sfx/turnoff.ogg" )
			$Surface.material_override = load( "res://Level/Tile_interactive_off_material.tres" )
			$AudioStreamPlayer.play()
		get_tree().call_group( "logic", "onLightToggled", mLit )

func _on_DetectionArea_body_entered( __ ):
	mBotOnTop = true

func _on_DetectionArea_body_exited( __ ):
	mBotOnTop = false

func reset():
	mLit = false
	$Surface.material_override = load( "res://Level/Tile_interactive_off_material.tres" )
