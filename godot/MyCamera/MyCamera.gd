extends Spatial

export var cameraSpeed = 0.1
export var maxZoom = 6.0
export var minZoom = 30.0
export var zoomStep = 1.0

onready var mCameraRotator = $CameraRotator
onready var mCamera = $CameraRotator/Camera
onready var mInitialPosition = global_transform.origin

var mZoom = minZoom

func _ready():
	setZoom( mZoom )
	mCameraRotator.rotation_degrees = Vector3( 0, -35, 35 )
	
func _unhandled_input( event ):
	if event is InputEventMouse:
		var mouse_event = event as InputEventMouse
		if Input.is_mouse_button_pressed( BUTTON_WHEEL_UP ):
			setZoom( mZoom - zoomStep )
		elif Input.is_mouse_button_pressed( BUTTON_WHEEL_DOWN ):
			setZoom( mZoom + zoomStep )
		
		if event is InputEventMouseMotion:
			var mouse_motion_event = event as InputEventMouseMotion
			var left_button = Input.is_mouse_button_pressed( BUTTON_LEFT )
			var middle_button = Input.is_mouse_button_pressed( BUTTON_MIDDLE )
			var right_button = Input.is_mouse_button_pressed( BUTTON_RIGHT )
			if left_button && !middle_button && !right_button:
				var motion = mouse_motion_event.relative
				mCameraRotator.rotation_degrees += Vector3( 0.0, cameraSpeed * -motion.x, cameraSpeed * motion.y )
			elif middle_button && !left_button && !right_button:
				var motion = mouse_motion_event.relative
				var y_dir = mCameraRotator.global_transform.origin.direction_to( mCamera.global_transform.origin )
				y_dir.y = mCameraRotator.global_transform.origin.y
				y_dir = y_dir.normalized()
				var x_dir = y_dir.rotated( Vector3.UP, deg2rad( 90.0 ) )
				mCameraRotator.global_transform.origin += y_dir * cameraSpeed * -motion.y + x_dir * cameraSpeed * -motion.x
			elif right_button && !left_button && !middle_button:
				mCameraRotator.global_transform.origin = mInitialPosition

func setZoom( zoom ):
		mZoom = clamp( zoom, maxZoom, minZoom )
		mCamera.translation.x = mZoom
