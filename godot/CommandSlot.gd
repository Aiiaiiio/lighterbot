extends TextureRect

onready var mSlotImage = $HBoxContainer/SlotImage
var mCommand = ""
var mBlacklistedCommands = []
var mHighlighted = false

signal commandChanged( command )

func setHighlighted( highlighted ):
	mHighlighted = highlighted
	if mHighlighted:
		self_modulate = Color( 0.0, 0.5, 0.0 )
	else:
		self_modulate = Color( 1.0, 1.0, 1.0 )

func command():
	return mCommand

func isEmpty():
	return mCommand == ""

func clear():
	if !isEmpty():
		mSlotImage.texture = null
		mCommand = ""
		emit_signal( "commandChanged", mCommand )
		
func setBlacklistedCommands( cmd_list ):
	mBlacklistedCommands = cmd_list

func can_drop_data( __, data ):
	return typeof( data ) == TYPE_DICTIONARY && data.has( "texture" ) && data.has( "command" ) && !mBlacklistedCommands.has( data[ "command" ] )

func drop_data( __, data ):
	mSlotImage.texture = data[ "texture" ]
	mCommand = data[ "command" ]

func _on_CommandSlot_gui_input( event ):
	if event is InputEventMouseButton:
		var mouse_button_event = event as InputEventMouseButton
		if mouse_button_event.pressed && mouse_button_event.button_index == BUTTON_RIGHT:
			clear()
