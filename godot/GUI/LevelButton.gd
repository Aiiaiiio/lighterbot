extends Panel

var mLevelScenePath = ""

onready var mButton = $VBoxContainer/Button
onready var mProgressLabel = $VBoxContainer/ProgressLabel

signal levelClicked( sender )

func setLevel( level_name, path ):
	mLevelScenePath = path
#	mButton.text = level_name # ?? null
	$VBoxContainer/Button.text = level_name
	if Flow.getProgressForLevel( path ):
#		mProgressLabel.text = "*"
		$VBoxContainer/ProgressLabel.text = "*"
	else:
#		mProgressLabel.text = ""
		$VBoxContainer/ProgressLabel.text = ""

func refreshProgress():
	if Flow.getProgressForLevel( mLevelScenePath ):
#		mProgressLabel.text = "*"
		$VBoxContainer/ProgressLabel.text = "*"
	else:
#		mProgressLabel.text = ""
		$VBoxContainer/ProgressLabel.text = ""

func levelScenePath():
	return mLevelScenePath
	
func levelName():
#	return mButton.text
	return $VBoxContainer/Button.text

func _on_Button_pressed():
	emit_signal( "levelClicked", self )
