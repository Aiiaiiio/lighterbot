extends Panel

onready var mProfileList = Flow.getUserProfileList()
onready var mCombobox = $HBoxContainer/ProfileList

signal profileSelected( profile_file_name )

func _ready():
	_onProfileListChanged()
	Flow.connect( "profileListChanged", self, "_onProfileListChanged" )
	Flow.connect( "profileChanged", self, "_onProfileChanged" )

func _on_ProfileList_item_selected( index ):
	if index >= 0 && index < mCombobox.get_item_count():
		var item_data = mCombobox.get_item_metadata( index )
#		emit_signal( "profileSelected", item_data )
		Flow.switchProfile( item_data )

func _onProfileListChanged():
	mCombobox.clear()
	mProfileList = Flow.getUserProfileList()
	for profile in mProfileList:
		mCombobox.add_item( profile[ "displayName" ] )
		mCombobox.set_item_metadata( mCombobox.get_item_count() - 1, profile[ "fileName" ] )
	_onProfileChanged()
	
func _onProfileChanged():
	var prev_file_name = ""
	if mCombobox.selected >= 0 && mCombobox.selected < mCombobox.get_item_count() - 1:
		prev_file_name = mCombobox.get_item_metadata( mCombobox.selected )
	
	var last_used_profile_filename = Flow.getFileNameForCurrentProfile()
	var last_used_profile_index = -1
	for idx in range( 0, mProfileList.size() ):
		var profile = mProfileList[ idx ]
		if last_used_profile_filename == profile[ "fileName" ]:
			last_used_profile_index = idx
			break
		
	if last_used_profile_index >= 0 && ( prev_file_name.empty() || ( prev_file_name != mCombobox.get_item_metadata( last_used_profile_index ) ) ):
		var prev_selected_idx = mCombobox.selected
		mCombobox.select( last_used_profile_index )
		if prev_selected_idx == mCombobox.selected:
			_on_ProfileList_item_selected( mCombobox.selected )

func _on_NewButton_pressed():
	$ProfileNameInputPopup.showDialog( "new" )

func _on_ProfileNameInputPopup_inputEntered():
	var input = $ProfileNameInputPopup.getInput()
	var request_id = $ProfileNameInputPopup.getRequestId()
	
	if !input.empty():
		if request_id == "new":
			var created_file_name = Flow.createNewProfile( $ProfileNameInputPopup.getInput() )
			if !created_file_name.empty():
				Flow.switchProfile( created_file_name )
		elif request_id == "rename":
			Flow.changeProfileName( input )

func _on_RenameButton_pressed():
	$ProfileNameInputPopup.showDialog( "rename", Flow.getDisplayNameForCurrentProfile() )

func _on_DeleteButton_pressed():
	$ConfirmationDialog.dialog_text = "Are you sure you would like to remove the profile named\n%s ?\n\nProgress will be lost (this is not reversable)!\n" % Flow.getDisplayNameForCurrentProfile()
	$ConfirmationDialog.popup_centered_minsize()

func _on_ConfirmationDialog_confirmed():
	Flow.removeCurrentProfile()
