extends Control

export var memorySizeMain : int = 10
export var memorySizeFn1 : int = 0
export var memorySizeFn2 : int = 0

func onExecution( callstack ):
	for i in range( 0, len( callstack ) - 1 ):
		var fnd = getFunctionDisplayById( callstack[i][0] )
		if fnd != null:
			fnd.setHighlightedIndex( callstack[i][1] - 1 )
	if !callstack.empty():
		var last = callstack.back()
		var fnd = getFunctionDisplayById( last[0] )
		if fnd != null:
			fnd.setHighlightedIndex( last[1] )
	
func onExecutionfinished( celebration_flag ):
	if celebration_flag:
		print( "Celebration finished" )
	else:
		for fnd in get_tree().get_nodes_in_group( "function_display" ):
			fnd.clearHighlight()
		print( "Program finished" )

func getFunctionDisplayById( idx ):
	for fnd in get_tree().get_nodes_in_group( "function_display" ):
		if fnd.id == idx:
			return fnd
	return null

func _ready():
	$Panel/VBoxContainer/MainFunc.maxCommands = memorySizeMain
	$Panel/VBoxContainer/Func1.maxCommands = memorySizeFn1
	$Panel/VBoxContainer/Func2.maxCommands = memorySizeFn2
	$Panel/VBoxContainer/FuncToolBar/ToolbarIcon.visible = memorySizeFn1 > 0
	$Panel/VBoxContainer/FuncToolBar/ToolbarIcon2.visible = memorySizeFn2 > 0

func _on_ClearButton_pressed():
	get_tree().call_group( "function_display", "clearCommands" )

func _on_ExecuteButton_pressed():
	get_tree().call_group( "logic", "resetLevel" )
	yield( get_tree(), "idle_frame" )
	
	get_tree().call_group( "bot", "clearProgram" )
	yield( get_tree(), "idle_frame" )
	
	get_tree().call_group( "function_display", "queueCommands" )
	yield( get_tree(), "idle_frame" )
	
	get_tree().call_group( "bot", "executeProgram" )

func _on_HomeButton_pressed():
	get_tree().change_scene( "res://GUI/MainScreen.tscn" )
