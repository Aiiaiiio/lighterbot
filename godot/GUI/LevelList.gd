extends Panel

onready var mLevelFileList = Flow.getLevelFileList()
onready var mLevelButtonScene = load( "res://GUI/LevelButton.tscn" )

var mButtons = []

func _ready():
	createButtons()
	Flow.connect( "progressChanged", self, "_onProgressChanged" )

func createButtons():
	for btn in mButtons:
		btn.queue_free()
	mButtons = []
	
	for idx in range( 0, mLevelFileList.size() ):
		var new_button = mLevelButtonScene.instance()
		new_button.setLevel( "Level " + str( idx + 1 ),  mLevelFileList[ idx ] )
		new_button.connect( "levelClicked", self, "onLevelClicked" )
		mButtons.append( new_button )
		$HBoxContainer/GridContainer.add_child( new_button )
		

func onLevelClicked( sender ):
	print( "Level %s clicked (%s)" % [ sender.levelName(), sender.levelScenePath() ] )
	Flow.startLevel( sender.levelScenePath() )

func _onProgressChanged():
	for btn in mButtons:
		btn.refreshProgress()
