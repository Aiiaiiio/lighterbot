extends Panel

export var displayedTitle = "Main"
export var id : int = 0
export var maxCommands = 10 setget _set_max_commands

var mSlots = []

func _ready():
	$VBoxContainer/Label.text = displayedTitle
	_populateSlots()
		
func queueCommands():
	for slot in mSlots:
		if !slot.isEmpty():
			get_tree().call_group( "bot", "queueCommand", id, slot.command() )
			
func clearCommands():
	for slot in mSlots:
		slot.clear()

func clearHighlight():
	for slot in mSlots:
		slot.setHighlighted( false )

func setHighlightedIndex( idx ):
	clearHighlight()
	var non_empty_idx = 0
	for slot in mSlots:
		if !slot.isEmpty():
			if non_empty_idx == idx:
				slot.setHighlighted( true )
				break
			else:
				non_empty_idx += 1

func _populateSlots():
	for slot in mSlots:
		slot.queue_free()
	mSlots = []
	
	var command_slot_type = load( "res://GUI/CommandSlot.tscn" )
	
	for i in range( 0, maxCommands ):
		var slot = command_slot_type.instance()
		$VBoxContainer/GridContainer.add_child( slot )
		slot.setBlacklistedCommands( [ "fn_%s" % id ] )
		mSlots.push_back( slot )
	
func _set_max_commands( max_commands ):
	maxCommands = max_commands
	_populateSlots()
	visible = max_commands > 0
