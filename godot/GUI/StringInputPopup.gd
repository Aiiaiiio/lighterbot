extends PopupDialog

signal inputEntered()
signal canceled()

export var question = "What do you need?"

var mAccepted = false
var mRequestId = null

func getInput():
	return $VBoxContainer/LineEdit.text

func isAccpeted():
	return mAccepted

func getRequestId():
	return mRequestId

func showDialog( request_id, pre_filled_text = "" ):
	mAccepted = false
	mRequestId = request_id
	$VBoxContainer/LineEdit.text = pre_filled_text
	popup_centered_minsize()

func _on_OkButton_pressed():
	mAccepted = true
	hide()
	emit_signal( "inputEntered" )

func _on_CancelButton_pressed():
	mAccepted = false
	hide()
	emit_signal( "canceled" )
