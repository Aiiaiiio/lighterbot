extends TextureButton

export var payload = ""
#
#func _on_ToolbarIcon_gui_input( event ):
#    if event is InputEventMouseButton:
#        var mouse_button_event = event as InputEventMouseButton
#        if mouse_button_event.button_index == BUTTON_LEFT:
#            get_tree().call_group( "logic", "doCommand", payload )

func get_drag_data( _pos ):
	if payload.empty() || payload == "execute":
		return null
		
	var icon = duplicate()
	set_drag_preview( icon )
	return { "texture": texture_normal, "command": payload }
